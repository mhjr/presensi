<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Presensi_guru extends CI_Controller
{

   function __construct()
   {
      // Construct the parent class
      parent::__construct();
	  $this->load->helper(['url', 'language', 'form']);
   }
   
   function masuk($id){
	   $curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => 'myinova.web.id/admin/api/presensi/'.$id,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'GET',
	  CURLOPT_HTTPHEADER => array(
		'x-api-key: bacc23504df63b28a83e679ea9dcd48b',
		'Authorization: Basic SU5PVkE6bWVydG95dWRhbg==',
		'Cookie: ci_session=65mses0bjihsm5j0409nca9hnll1o5rk'
	  ),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	$respon = json_decode($response);
	if($respon[0]->status == 1){
		redirect('https://myinova.web.id/admin');
	} else {
		echo 'Gagal';
	}
   }
  
 
}
